# Fieldlab hostingomgeving

De hostingomgeving voor de Fieldlab WOZ-omgeving wordt beheerd door Digilab. De omgeving draait in Azure op Kubernetes. Als jij software bouwt is het mogelijk dit de draaien in deze omgeving. Hoe dat moet, lees je hieronder.

De omgeving maakt gebruik van GitOps d.m.v. Flux. Hiermee kun je Kubernetes manifests in een repository plaatsen en Flux zal deze vanzelf toepassen op het cluster. Om de manifests gemakkelijk te kunnen beheren en genereren wordt er gebruik gemaakt van kustomize. Dit zit tegenwoordig ingebakken in `kubectl`.

## Een applicatie toevoegen
Digilab heeft [een repository](https://gitlab.com/digilab.overheid.nl/platform/flux/tenant-digilab-fieldlab) opgezet waarin je de manifests voor je applicatie kunt zetten. Om je manifests hierin te krijgen, doe je het volgende:

1.	Fork de repository.
2.	Voeg je manifests toe volgens de structuur van de voorbeeldapplicatie `hello-world`. Als je applicatie uit meerdere services bestaat, kun je onder de `base` map submappen hiervoor aanmaken, zie ook het voorbeeld. In de `overlays` map maak je een map `sandbox` aan met hierin omgevings-specifieke configuratie. Ook kun je hier versleutelde `secrets` toevoegen, zie hieronder hoe dat werkt.
3.	Maak een Merge Request aan op de originele repository. Deze wordt dan door Digilab gereviewed en doorgezet.
4.	Na het mergen zijn de wijzigingen binnen een minuut doorgevoerd op de omgeving.

## Eisen applicatie
Om je applicatie te kunnen draaien om de Fieldlab omgeving, moet deze aan de volgende eisen voldoen:

- Elke workload moet gecontaineriseerd zijn, d.w.z. kunnen draaien in een (Docker) container.
- Elke container heeft een enkele functie, er mogen niet meerdere services draaien in één container.
- De workloads moeten stateless zijn, dus deze moeten niet schrijven naar hun local filesystem. Er kan wel geschreven worden naar databases en object storage.
- De container images dienen in een publiek beschikbaar registry te staan. Dit mag wel afgeschermd zijn met authenticatie. Het Digilab team kan hierin eventueel helpen.
- Gebruikte poorten moeten hoger zijn dan `1024`. Voor open source software kun je meestal de standaardpoort gebruiken, zoals `5432` voor PostgrSQL of `6379` voor Redis. Voor webapplicaties gebruik je bijv. `8000` of `8080`, maar hier ben je vrij in. De bijbehorende service heeft in het eerste geval dezelfde poort, maar in het tweede geval (webapplicatie) altijd poort `80`.
- Elke workload beschikt over een [livenessProbe en een readinessProbe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/). Voor een webapplicatie moeten dit een lichtgewicht call zijn, bijvoorbeeld een simpele pagina die enkel een correcte statuscode teruggeeft.
- Voor elke workload zijn `resource requests` en `resource limits` geconfigureerd, voor zowel `cpu` als `memory`.

## Applicatie beschikbaar maken over internet
Indien het nodig is je applicatie via het internet beschikbaar te maken, bijvoorbeeld voor een frontend, dan kun je een `Ingress` object toevoegen, zie ook het voorbeeld. Zorg ervoor dat je de Let’s Encrypt annotation meegeeft zodat je automatisch een HTTPS-certificaat uitgedeeld krijgt. Je kunt gebruik maken van de wildcard *.apps.digilab.network, of een eigen domeinnaam, maar dan moet je wel zelf zorg dragen dat de DNS goed staan. Je dient hiervoor een CNAME-record aan te maken naar `lb.apps.digilab.network`

## Secrets versleutelen
Aangezien het GitOps-principe voorschrijft alle Kubernetes resources beschreven te hebben in een Git repository en het best-practice is geen secrets in te checken in een repository (en uiteraard noodzakelijk als de repository publiek is, zoals deze), maken we gebruik van asymmetrische encryptie. Dit maakt het makkelijk om secrets te versleutelen d.m.v. een publieke sleutel. In het cluster, in jouw specifieke namespace, staat een privé-sleutel waarmee het ontsleuteld kan worden. Wij zorgen ervoor dat in de specifieke overlay een publieke sleutel komt te staan die je voor het versleutelen kunt gebruiken. We maken hiervoor gebruik van SOPS . Je versleuteld een Kubernetes secret als volgt:

```bash
sops \
  --encrypt \
  --encrypted-regex '^(data|stringData)$' \
  --age $(cat [jouw-key].pub) \
  --in-place \
  my-secret.yaml
```

Na versleuteling is het veilig het versleutelde bestand in te checken.

## Communicatie tussen services
Als jouw service moet communiceren met een andere service, is daar een aantal mogelijkheden voor:

- Directe communicatie via Kubernetes services binnen jouw eigen namespace. Dit is mogelijk.
- Directe communicatie via Kubernetes service buiten jouw eigen namespace. Dit is niet mogelijk vanwege Network Policies die alleen verkeer binnen de namespace toestaan.
- Via [NLX](https://nlx.io). Dit is de aanbevolen manier. Voor Fieldlab draaien we [vijf NLX instanties](https://fds-fieldlab.apps.digilab.network) voor organisaties.
- Via internet. Als de service beschikbaar is over het internet, kun je er direct mee communiceren, dit is niet geblokkeerd. Draag hierbij wel zelf zorg voor de beveiliging ervan.

## Database
Mocht je een database nodig hebben, dan ben je vrij hier zelf de `Deployment` of `StatefulSet` voor aan te maken. Persistent volumes zijn beschikbaar. Als je PostgreSQL nodig hebben, dan hebben we hiervoor de [Cloud Native PostgreSQL (CNPG) Operator](https://cloudnative-pg.io) beschikbaar, waardoor je met onderstaande manifest een PostgreSQL instantie kunt aanmaken:

```yaml
apiVersion: postgresql.cnpg.io/v1
kind: Cluster
metadata:
  name: mydb
spec:
  instances: 1
  storage:
    size: 10Gi
  bootstrap:
    initdb:
      database: mydb
      owner: mydb

```

Vervolgens wordt er een wachtwoord gegenereerd en in een secret genaamd `mydb-app` geplaatst. Hierin zit een key `username` en `password`, welke je in kunt laden in je `Deployment`. De hostname van de service is `mydb-rw` en de poort `5432`.

Er worden geen backups gemaakt van de database en/of persistent volumes.

## Operators
Het is niet toegestaan operators te installeren in het cluster. De enige die beschikbaar is, is de hierboven genoemen CNPG operator voor PostgreSQL databases.

## Image registry
Je dient zelf zorg te dragen voor een plek waar je de gebouwde images van je applicatie kwijt kunt. Dit kan bijv. op Docker Hub, Gitlab of elders. Hier mag authenticatie op zitten, maar zorg dan wel dat je deze als versleuteld manifest toevoegt en de `imagePullSecrets` voor je workloads configureert.

## Applicatie repository
Je kunt je eigen repository gebruiken voor de code van je applicatie.
